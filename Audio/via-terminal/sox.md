```
sudo apt update
```

```
sudo apt install sox
```

```
sudo apt-get install libsox-fmt-mp3
```


[Official page](http://sox.sourceforge.net/)

[Examples](http://sox.sourceforge.net/sox.html)


Maximum possible volume without clipping (distortion)
```
sox --norm female.mp3 female.normalized.mp3
```
